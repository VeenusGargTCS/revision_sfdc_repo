@isTest
public class RevClassContactTest{
    @isTest
    public static void getContactTest(){
        Contact c = new Contact();
        c.LastName ='GARGV';
        insert c;
        test.StartTest();
            RevClassContact revCl = new RevClassContact();
            Contact ct = revCl.getContact();
        test.StopTest();
        System.assertEquals('GARGV', ct.LastName,'Return Last Name');
    }
}