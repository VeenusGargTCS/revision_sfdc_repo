@isTest
public class MyControllerClass1Test{
    @isTest
    public static void getContactTest(){
        Contact c = new Contact();
        c.LastName ='GARGV';
        insert c;
        test.StartTest();
            MyControllerClass1 revCl = new MyControllerClass1();
            Contact ct = revCl.getContact();
        test.StopTest();
        System.assertEquals('GARGV', ct.LastName,'Return Last Name');
    }
}