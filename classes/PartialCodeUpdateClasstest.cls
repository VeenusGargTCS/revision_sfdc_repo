@isTest
public class PartialCodeUpdateClasstest{
    @isTest
    public static void getContactTest(){
        Contact c = new Contact();
        c.LastName ='GARGV';
        insert c;
        test.StartTest();
            PartialCodeUpdateClass revCl = new PartialCodeUpdateClass();
            Contact ct = revCl.getContact();
        test.StopTest();
        System.assertEquals('GARGV', ct.LastName,'Return Last Name');
    }
}